# Contributing

Everyone can contribute to this project in different ways like writing code, creating layouts, designing the icons, suggesting acceptable changes, translating it to their native language, writing documentation, improving the website etc. 

### Code contributaion:
Fork this repository. Pull the forked repository from your namespace to your local machine. Create new branch and work on the bug/feature/enhacement you would like to submit. Push it to your forked version. From there create Merge Request(MR) against **master** branch.

### Translations:
Help us translate it to your native language. Take a look [here](https://gitlab.com/mmarif4u/gitnex/blob/master/app/src/main/res/values/strings.xml) for strings, please ignore the lines with `translatable="false"`. It is recommended to create a Merge Request with your changes. If it's not your thing, share the translated `strings.xml` via an issue. Check the structure of other languages for example [French](https://gitlab.com/mmarif4u/gitnex/blob/master/app/src/main/res/values-fr/strings.xml). 

