[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[<img alt="Become a Patroen" src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" height="40"/>](https://www.patreon.com/mmarif)  [![Libera patrons](https://img.shields.io/liberapay/patrons/mmarif.svg?logo=liberapay)](https://liberapay.com/mmarif/donate)  [<img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"/>](https://liberapay.com/mmarif/donate)

# GitNex - Android client for Gitea

GitNex is a free, open-source Android client for Git repository management tool Gitea. Gitea is a community managed fork of Gogs, lightweight code hosting solution written in Go.

GitNex is licensed under GPLv3 License. See the LICENSE file for the full license text.
No trackers are used and source code is available here for anyone to audit.

## Downloads
[<img alt='Get it on F-droid' src='https://gitlab.com/fdroid/artwork/raw/master/badge/get-it-on.png' height="80"/>](https://f-droid.org/en/packages/org.mian.gitnex/) [<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height="80"/>](https://play.google.com/store/apps/details?id=org.mian.gitnex) [Download APK](https://gitlab.com/mmarif4u/gitnex/releases)

## Note about Gitea version
Please make sure that you are on Gitea **1.6.x** stable release or later. Below this may not work as one would expect because of the newly added objects to the API at later versions. Please consider updating your Gitea server.

Check the versions [compatibility page](https://gitlab.com/mmarif4u/gitnex/wikis/Compatibility) which lists all the supported versions with compatibility ratio.

## Build from source
Option 1 - Download the source code, open it in Android Studio and build it there.

Option 2 - Open terminal(Linux) and cd to the project dir. Run `./gradlew build`.

## Features
- My Repositories
- Repositories list
- Organizations list
- Create new repository
- Create new organization
- AND MORE ...

For complete list, [check features page](https://gitlab.com/mmarif4u/gitnex/wikis/Features).

## Contributing
[CONTRIBUTING](https://gitlab.com/mmarif4u/gitnex/blob/master/CONTRIBUTING.md)

## Screenshots:
[Screenshots](https://gitlab.com/mmarif4u/gitnex/wikis/Screenshots)

## FAQ
[Faq](https://gitlab.com/mmarif4u/gitnex/wikis/FAQ)

## Links
[Website](https://gitnex.com)

[Wiki](https://gitlab.com/mmarif4u/gitnex/wikis/home)

[Website Repository](https://gitlab.com/mmarif4u/gitnex-website)

## Thanks
Thanks to all the open source libraries, contributors and donators.

Open source libraries
- Retrofit
- Gson
- Okhttp
- ViHtarb/tooltip
- Picasso
- Markwon
- Prettytime
- Amulyakhare/textdrawable
- Vdurmont/emoji-java
- Abumoallim/android-multi-select-dialog
- Pes/materialcolorpicker

[Follow me on Fediverse](https://mastodon.social/@mmarif)
